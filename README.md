# L2_CDA_ModuleImage

## Description
Module Image LIFPACD

Pour compiler: CMakelists.txt

Organisation: 
- src : code source
- doc : documentation
- build : dossier de compilation
- bin : dossier des exécutables
    - bin/exemple : dossier des exécutables des exemples
    - bin/test : dossier des exécutable des tests
    - bin/affichage : dossier des exécutable de l'affichage
- lib : dossier des librairies
- data : dossier des images
- CMakeLists.txt : fichier de configuration de compilation
- README.md : fichier de description du projet

ID Forge: 33086
Antoine FOUR p2207715
Anes ERRIH p2310055
