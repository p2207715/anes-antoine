#include "Image.h"
#include <fstream>
#include <cassert>
#include <iostream>
#include <ostream>
using namespace std;

Image::Image() {
    dimx = 0;
    dimy = 0;
}

Image::Image(unsigned int dim_x, unsigned int dim_y) {
    assert(dim_x > 0 && dim_y > 0);
    dimx = dim_x;
    dimy = dim_y;
    tab = new Pixel[dimx*dimy];
}

Image::~Image() {
    delete[] tab;
}

bool Image::isValid(unsigned int x, unsigned int y) const  {
    return x <= dimx && y <= dimy;
}

Pixel Image::getPix(unsigned int x, unsigned int y) {
    assert(isValid(x,y));
    return tab[y * dimx + x];
}

Pixel Image::getPix(unsigned int x, unsigned int y) const {
    assert(isValid(x,y));
    return Pixel(tab[y * dimx + x]);
}

void Image::setPix(unsigned int x, unsigned int y, Pixel couleur) {
    assert(isValid(x,y));
    tab[y*dimx+x] = couleur;
}

void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur) {
    for (int i = Xmin; i < Xmax; i++) {
        for (int j = Ymin; j < Ymax; j++) {
            setPix(i, j, couleur);
        }
    }
}

void Image::effacer(Pixel couleur) {
    dessinerRectangle(0,0,dimx,dimy, couleur);
}

void Image::testRegression() {
    Image image = Image(10, 10);
    Pixel pixel = Pixel(255, 255, 255);

    image.dessinerRectangle(0, 0, 5, 5, pixel);

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            assert(image.getPix(i, j).r == 255 && image.getPix(i, j).g == 255 && image.getPix(i, j).b == 255);
        }
    }
    image.setPix(0, 0, Pixel());

    assert(image.getPix(0, 0).r == 0 && image.getPix(0, 0).g == 0 && image.getPix(0, 0).b == 0);
}

void Image::sauver(const string &filename) const {
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; y++) {
        for (unsigned int x = 0; x < dimx; x++) {
            const Pixel &pix = getPix(x, y);
            fichier << + pix.r << " " << + pix.g << " " << + pix.b << " ";
        }
    }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename){
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    unsigned r, g, b;
    string chaine;
    dimx = dimy = 0;
    fichier >> chaine >> dimx >> dimy >> chaine;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> g >> b;
            Pixel pixel1(r,g,b);
            setPix(x,y,pixel1);
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole() {
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            const Pixel &pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}
