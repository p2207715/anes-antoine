#ifndef PROCONCEPTION_IMAGE_H
#define PROCONCEPTION_IMAGE_H
#include "Pixel.h"
#include "string"

/**
 * @class Image
 * @brief Classe representant une image composé de @ref Pixel pixels
 */
class Image {
private:
    Pixel * tab;
    unsigned int dimx,dimy;
public:
    /**
     * @brief Constructeur par défaut créant une image de dimension 0 sans allouer de mémoire pour le tableau
     */
    Image();

    /**
     * @brief Constructeur créant une image de dimension dim_x * dim_y et allouant la mémoire pour le tableau de pixels
     * Après verification que les dimensions sont strictement positives
     * @param unsigned int dim_x
     * @param unsigned int dim_y
     */
    Image(unsigned int dim_x, unsigned int dim_y);

    /**
     * @brief Destructeur libérant la mémoire allouée pour le tableau de pixels
     */
    ~Image();

    /**
     * @brief Méthode permettant de vérifier si les coordonnées passées en paramètres sont valides
     * @param unsigned int x
     * @param unsigned int y
     * @return bool
     */
    bool isValid(unsigned int x, unsigned int y) const;

    /**
     * @brief Méthode permettant de récupérer le pixel aux coordonnées passées en paramètres
     * @param unsigned int x
     * @param unsigned int y
     * @return Pixel
     */
    Pixel getPix(unsigned int x, unsigned int y);

    /**
     * @brief Méthode permettant de récupérer une copie du pixel aux coordonnées passées en paramètres
     * @param unsigned int x
     * @param unsigned int y
     * @return Pixel
     */
    Pixel getPix(unsigned int x , unsigned int y) const;

    /**
     * @brief Méthode permettant de modifier le pixel aux coordonnées passées en paramètres
     * @param unsigned int x
     * @param unsigned int y
     * @param Pixel couleur
     */
    void setPix(unsigned int x, unsigned int y, Pixel couleur);

    /**
     * @brief Méthode permettant de dessiner un rectangle de couleur passée en paramètre
     * @param int Xmin
     * @param int Ymin
     * @param int Xmax
     * @param int Ymax
     * @param Pixel couleur
     */
    void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur);

    /**
     * @brief Méthode permettant d'effacer l'image avec la couleur passée en paramètre en utilisant la méthode dessinerRectangle
     * @param couleur
     */
    void effacer(Pixel couleur);

    /**
     * @brief Méthode permettant de tester la classe Image
     */
    void static testRegression();

    /**
     * @brief Méthode permettant de sauvegarder l'image dans un fichier
     * @param filename
     */
    void sauver(const std::string &filename) const;

    /**
     * @brief Méthode permettant d'ouvrir un fichier image
     * @param filename
     */
    void ouvrir(const std::string &filename);

    /**
     * @brief Méthode permettant d'afficher l'image dans la console
     */
    void afficherConsole();
};
#endif //PROCONCEPTION_IMAGE_H


