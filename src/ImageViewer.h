#ifndef TDIMAGE_IMAGEVIEWER_H
#define TDIMAGE_IMAGEVIEWER_H

#include <SDL2/SDL.h>
#include "Image.h"

/**
 * @class ImageViewer
 * @brief Classe permettant d'afficher une image
 */
class ImageViewer {
public:
    /**
     * @brief Constructeur par défaut
     */
    ImageViewer();

    /*$*
     * @brief Destructeur
     */
    ~ImageViewer();

    /**
     * @brief Méthode permettant d'afficher une image
     * @param image
     */
    void afficher(Image image);
};

#endif //TDIMAGE_IMAGEVIEWER_H
