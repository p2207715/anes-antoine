#include <iostream>
#include "Image.h"

using namespace std;

int main() {
    cout << "Début test regression..." << endl;

    Image::testRegression();

    cout << "Test regression réussi!" << endl;
}