#ifndef PROCONCEPTION_PIXEL_H
#define PROCONCEPTION_PIXEL_H

/**
 * @struct Pixel
 * @brief Structure representant un Pixel RGB
 */
struct Pixel {
    unsigned char r;
    unsigned char g;
    unsigned char b;

    /**
     * @brief Constructeur par défaut mettant le RGB à 0 donc noir
     */
    Pixel() : r(0), g(0), b(0) {}

    /**
     * @brief Constructeur par copie mettant le RGB à celui du pixel passé en paramètre
     * @param Pixel pixel
     */
    Pixel(const Pixel &pixel) : r(pixel.r), g(pixel.g), b(pixel.b) {}

    /**
     * @brief Constructeur mettant le RGB à celui passé en paramètre
     * @param unsigned char red
     * @param unsigned char green
     * @param unsigned char blue
     */
    Pixel(unsigned char red, unsigned char green, unsigned char blue) : r(red), g(green), b(blue) {}

};

#endif //PROCONCEPTION_PIXEL_H
